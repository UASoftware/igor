import { Inject, Injectable } from '@angular/core';
import { map, withLatestFrom } from 'rxjs/operators';

import { PROJECTS_REPOSITORY_TOKEN, ProjectsRepository } from '../repositories';

@Injectable({ providedIn: 'root' })
export class IsHaveToRateQuery implements Query<boolean> {
  pending$ = this.projectsRepository.loading$.pipe(
    withLatestFrom(this.evaluationsRepository.loading$),
    map(([isProjectsLoading, isEvaluationsLoading]) => isProjectsLoading || isEvaluationsLoading)
  );

  values$ = this.projectsRepository.isProjectOwner$.pipe(
    withLatestFrom(this.evaluationsRepository.isRated$),
    map(([isOwner, isRated]) => !isOwner || !isRated)
  );

  constructor(
    @Inject(PROJECTS_REPOSITORY_TOKEN)
    private projectsRepository: ProjectsRepository,
    @Inject(EVALUATIONS_REPOSITORY_TOKEN)
    private evaluationsRepository: EvaluationsRepository
  ) {}

  execute(value?: undefined) {}
}
