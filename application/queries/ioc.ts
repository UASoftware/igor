import { InjectionToken } from '@angular/core';

export const IS_HAVE_TO_RATE_QUERY_TOKEN = new InjectionToken<Query<boolean>>('IsHaveToRateQuery');

// ......
