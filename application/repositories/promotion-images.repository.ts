import { InjectionToken } from '@angular/core';
import { Observable } from 'rxjs';

export interface PromotionImagesRepository
  extends LoadableEntitiesRepository,
    StatusableEntitiesRepository,
    FallibleEntitiesRepository,
    CollectableEntitiesRepository<PromotionalImage> {
  activeOnly$: Observable<PromotionalImage[]>;
}

export const PROMOTIONAL_IMAGES_REPOSITORY_TOKEN = new InjectionToken<PromotionImagesRepository>(
  'PromotionImagesRepository'
);
