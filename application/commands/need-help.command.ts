import { Inject, Injectable } from '@angular/core';
import { iif } from 'rxjs';
import { switchMap, take } from 'rxjs/operators';
import { observableEither as OE } from 'fp-ts-rxjs';
import { WINDOW } from '@ng-web-apis/common';

import { PROJECTS_REPOSITORY_TOKEN, ProjectsRepository } from '../repositories';

@Injectable({ providedIn: 'root' })
export class NeedHelpCommand extends NavigationCommand {
  constructor(
    @Inject(PROJECTS_REPOSITORY_TOKEN)
    private projectsRepository: ProjectsRepository,
    @Inject(FAQ_URL_PROVIDER_TOKEN)
    private faqUrlProvider: FAQUrlProvider,
    @Inject(WINDOW) window: Window
  ) {
    super(window);
  }

  public onExecute(model?: undefined) {
    return this.projectsRepository.current$.pipe(
      take(1),
      switchMap((project) =>
        iif(
          () => !!project?.order,
          this.openUrlInNewTab(this.faqUrlProvider(project!.order!.id, project!.id)),
          OE.left(new ProvideUrlFailedDueToInvalidArgumentError())
        )
      )
    );
  }
}
