import { InjectionToken } from '@angular/core';

export const NEED_HELP_COMMAND_TOKEN = new InjectionToken<Command<undefined, boolean>>(
  'NeedHelpCommand'
);

// ......
