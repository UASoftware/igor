@Injectable({ providedIn: 'any' })
export class TipsPresenter extends ShuffledCards<TipsCard, number> {
  private readonly insertAdsCardEveryRows = 2;
  private readonly defaultNumberColumns = 1;

  constructor(
    @Inject(ARTICLES_QUERY_TOKEN)
    private articlesQuery: Query<Article[]>,
    @Inject(PROMOTIONAL_IMAGES_QUERY_TOKEN)
    private advertiseCardsQuery: Query<AdvertiseCard[], PromotionalImagesLanding>
  ) {
    super();
    this.advertiseCardsQuery.execute('articles');
  }

  public onQuery(criteria?: number): OE.ObservableEither<CustomError, TipsCard[]> {
    return combineLatest([this.articlesQuery.values$, this.advertiseCardsQuery.values$]).pipe(
      map(([articles, advertiseCards]) => {
        const shuffledCards = this.shuffleCards(
          articles,
          advertiseCards,
          criteria || this.defaultNumberColumns
        );
        return E.right(shuffledCards);
      })
    );
  }

  // .....
}
