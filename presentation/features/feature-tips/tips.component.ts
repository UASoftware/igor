@Component({
  selector: 'aex-tips',
  templateUrl: './tips.component.html',
  styleUrls: ['./tips.component.scss'],
  providers: [TipsPresenter, { provide: INITIAL_COLUMNS_TOKEN, useValue: 3 }],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TipsComponent {
  shuffledCards$ = this.shuffledCardsPresenter.values$;

  constructor(
    private shuffledCardsPresenter: TipsPresenter,
    @Inject(GO_TO_ARTICLE_TIP_COMMAND_TOKEN)
    private openArticleTips: Command<Article, boolean>,
    @Inject(GO_TO_ADVERTISING_WINDOW_COMMAND_TOKEN)
    private openAdvertising: Command<AdvertiseCard, boolean>,
    @Inject(INITIAL_COLUMNS_TOKEN) initialColumns: number | undefined
  ) {
    this.shuffledCardsPresenter.execute(initialColumns);
  }

  // .....
}
