import {
  Component,
  ComponentFactoryResolver,
  ViewChild,
  ChangeDetectionStrategy,
  Inject,
  OnInit,
} from '@angular/core';
import { FormGroupState } from 'ngrx-forms';
import { Observable } from 'rxjs';

@Component({
  selector: 'basic-sign-in',
  templateUrl: './feature-basic-sign-in.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BasicSignInComponent implements OnInit {
  @ViewChild(AnyHostDirective, { static: true }) headerHost: AnyHostDirective;

  formState$: Observable<FormGroupState<BasicCredentials>>;
  status$: Observable<StateStatus>;
  submitBtnText: string;

  constructor(
    private componentFactoryResolver: ComponentFactoryResolver,
    @Inject(BASIC_SIGN_IN_CONFIG_TOKEN) private config: BasicSignInConfig,
    @Inject(BASIC_SIGN_IN_FACADE_TOKEN) private facade: BasicSignInFacade
  ) {
    this.formState$ = facade.formState$;
    this.status$ = facade.status$;
    this.submitBtnText = config.submitBtnText;
  }

  ngOnInit() {
    if (this.config && this.config.headerComponent) {
      this.createHeaderComponent(this.config.headerComponent);
    }
  }

  private createHeaderComponent(
    hostComponentConfig: HostComponentConfig<unknown, HostComponent<unknown>>
  ) {
    const componentFactory = this.componentFactoryResolver.resolveComponentFactory(
      hostComponentConfig.component
    );

    const viewContainerRef = this.headerHost.viewContainerRef;
    viewContainerRef.clear();

    const componentRef = viewContainerRef.createComponent(componentFactory);
    componentRef.instance.data = hostComponentConfig.data;
  }

  onSubmit() {
    this.facade.submit();
  }
}
