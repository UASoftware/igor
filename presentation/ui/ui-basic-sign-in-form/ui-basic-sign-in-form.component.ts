import { Component, Input, Output, EventEmitter, ChangeDetectionStrategy } from '@angular/core';
import { FormGroupState } from 'ngrx-forms';
import { Observable } from 'rxjs';

@Component({
  selector: 'basic-sign-in-form',
  templateUrl: './ui-basic-sign-in-form.component.html',
  styleUrls: ['./ui-basic-sign-in-form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BasicSignInFormComponent {
  @Input() submitBtnText: string;
  @Input() formState$: Observable<FormGroupState<BasicCredentials>>;
  @Input() status$: Observable<StateStatus>;

  @Output() submitForm: EventEmitter<void> = new EventEmitter();

  onSubmit() {
    this.submitForm.emit();
  }
}
