import { Component, Inject, TemplateRef, ViewChild, ViewContainerRef } from '@angular/core';
import { Overlay, OverlayRef } from '@angular/cdk/overlay';
import { TemplatePortal } from '@angular/cdk/portal';
import { merge } from 'rxjs';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';

@UntilDestroy()
@Component({
  selector: 'shell-root',
  template: `
    <div fxLayout="column" fxFlexFill>
      <top-bar *ngIf="topBarVisible$ | async"></top-bar>
      <div fxFlex class="outlet-container">
        <router-outlet></router-outlet>
      </div>
    </div>
    <ng-template #burgerMenuTemplateRef>
      <burger-menu></burger-menu>
    </ng-template>
    <routable-dialog></routable-dialog>
  `,
})
export class ShellRootMobileComponent {
  @ViewChild('burgerMenuTemplateRef') burgerMenuTemplateRef?: TemplateRef<unknown>;

  topBarVisible$ = this.navBarsService.visible$;

  private overlayRef?: OverlayRef;

  constructor(
    private overlay: Overlay,
    private viewContainerRef: ViewContainerRef,
    private navBarsService: NavBarsService,
    @Inject(OVERLAYS_REPOSITORY_TOKEN)
    private overlaysRepository: OverlaysRepository
  ) {
    this.overlaysRepository.isBurgerMenuVisible$
      .pipe(untilDestroyed(this))
      .subscribe((visible) => (visible ? this.showBurgerMenu() : this.hideBurgerMenu()));
  }

  showBurgerMenu() {
    if (this.burgerMenuTemplateRef) {
      this.overlayRef = this.overlay.create({
        positionStrategy: this.overlay
          .position()
          .flexibleConnectedTo({ x: 0, y: 0 })
          .withPositions([
            {
              originX: 'center',
              originY: 'bottom',
              overlayX: 'center',
              overlayY: 'top',
            },
          ]),
        scrollStrategy: this.overlay.scrollStrategies.noop(),
        hasBackdrop: true,
        panelClass: 'burger-menu-dialog',
      });

      const templatePortal = new TemplatePortal(this.burgerMenuTemplateRef, this.viewContainerRef);

      this.overlayRef.attach(templatePortal);

      merge(this.overlayRef.backdropClick(), this.overlayRef.detachments())
        .pipe(untilDestroyed(this))
        .subscribe(() => {
          this.hideBurgerMenu();
          this.overlaysRepository.hideBurgerMenu();
        });
    }
  }

  hideBurgerMenu() {
    if (this.overlayRef) {
      this.overlayRef.dispose();
      this.overlayRef = undefined;
    }
  }
}
