import { ModuleWithProviders, NgModule } from '@angular/core';

@NgModule()
export class DataAccessProvidingModule {
  static forRoot(): ModuleWithProviders<DataAccessProvidingModule> {
    return {
      ngModule: DataAccessProvidingModule,
      providers: [
        {
          provide: ADD_PROPOSAL_URL_PROVIDER_TOKEN,
          useFactory: (environment: Environment): AddProposalUrlProvider => (
            orderId: number,
            projectId: string
          ) => `${environment.api.orderUrl}aaa/${orderId}/bbb/${projectId}/ccc`,
          deps: [ENV_TOKEN],
        },
        {
          provide: CREATE_NEW_PROJECT_URL_PROVIDER_TOKEN,
          useFactory: (environment: Environment): CreateNewProjectUrlProvider => () =>
            `${environment.api.createNewProjectUrl}`,
          deps: [ENV_TOKEN],
        },
      ],
    };
  }
}
