import { Inject, Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable } from 'rxjs';
import { observableEither as OE } from 'fp-ts-rxjs';
import { CustomError } from 'ts-custom-error';

@Injectable()
export class ApiKeyInterceptor implements HttpInterceptor {
  constructor(
    private apiKeyService: ApiKeyService,
    @Inject(ENV_TOKEN) private environment: Environment
  ) {}

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    return this.hasApiKey(request)
      ? next.handle(request.clone())
      : OE.fold<CustomError, string, HttpEvent<unknown>>(
          () => next.handle(request.clone()),
          (apiKey) =>
            next.handle(
              request.clone({
                params: request.params.set(this.environment.queryParams.apiKey, apiKey),
              })
            )
        )(this.apiKeyService.getApiKey());
  }

  private hasApiKey(request: HttpRequest<unknown>) {
    return request.url.toLowerCase().includes(`${this.environment.queryParams.apiKey}=`);
  }
}
