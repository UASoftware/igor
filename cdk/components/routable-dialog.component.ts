import { Component, OnInit, Inject, Optional, Type } from '@angular/core';
import { ActivatedRoute, NavigationStart, Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { of } from 'rxjs';
import { catchError, filter, map, switchMap, take, timeout } from 'rxjs/operators';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';

import { ROUTABLE_COMPONENT_REF_TOKEN, RoutableComponentRef } from './routable-component-ref';

@UntilDestroy()
@Component({
  selector: 'routable-dialog',
  template: `<ng-template></ng-template>`,
})
export class RoutableDialogComponent implements OnInit {
  private readonly timeoutBeforeCatchErrorInMs = 100;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private dialog: MatDialog,
    @Optional() @Inject(ROUTABLE_COMPONENT_REF_TOKEN) private componentRefs: RoutableComponentRef[]
  ) {}

  ngOnInit() {
    if (this.componentRefs) {
      this.route.queryParamMap
        .pipe(
          untilDestroyed(this),
          map((paramMap) =>
            this.componentRefs
              .filter((ref) => paramMap.has(ref.queryParam))
              .map((ref) => ({ componentRef: ref, param: paramMap.get(ref.queryParam) }))
          ),
          filter((values) => values.length > 0)
        )
        .subscribe((values) => {
          values.forEach(({ componentRef, param }) => {
            if (!this.isDialogAlreadyOpened(componentRef)) {
              if (componentRef.loadComponent) {
                this.openDialogLazily(componentRef, param);
              } else if (componentRef.component) {
                this.openDialog(componentRef.component, componentRef, param);
              }
            }
          });
        });
    }
  }

  private openDialogLazily<ComponentType, DataType>(
    componentRef: RoutableComponentRef<ComponentType, DataType>,
    queryParamValue?: string | null
  ) {
    componentRef.loadComponent!().then((component) =>
      this.openDialog(component, componentRef, queryParamValue)
    );
  }

  private openDialog<ComponentType, DataType>(
    component: Type<ComponentType>,
    componentRef: RoutableComponentRef<ComponentType, DataType>,
    queryParamValue?: string | null
  ) {
    const { queryParam, options, data } = componentRef;

    const parsedParam = this.tryToParseQuery(queryParamValue);

    const dialogRef = this.dialog.open(component, {
      ...options,
      id: queryParam,
      data: data ?? parsedParam,
    });

    dialogRef
      .afterClosed()
      .pipe(
        untilDestroyed(this),
        switchMap(() =>
          this.router.events.pipe(
            timeout(this.timeoutBeforeCatchErrorInMs),
            filter((event) => event instanceof NavigationStart),
            take(1),
            catchError(() => of(undefined))
          )
        )
      )
      .subscribe((result) => {
        if (!result) {
          this.router.navigateByUrl(this.removeQuery(queryParam, this.router.url), {
            replaceUrl: true,
          });
        }
      });
  }

  private isDialogAlreadyOpened<ComponentType, DataType>(
    componentRef: RoutableComponentRef<ComponentType, DataType>
  ) {
    return this.dialog.getDialogById(componentRef.queryParam);
  }

  // ......
}
