import { Directive, Input, TemplateRef, ViewContainerRef } from '@angular/core';
import { BreakpointObserver } from '@angular/cdk/layout';
import { BehaviorSubject, combineLatest, Subject } from 'rxjs';
import { distinctUntilChanged, map, switchMap, withLatestFrom } from 'rxjs/operators';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';

import { MediaBreakpoint } from './media-breakpoint.union';
import { MediaQueryFilterStrategy } from './media-query-filter-strategy.union';

type MediaQueryBreakpointType = MediaBreakpoint | readonly MediaBreakpoint[];

@UntilDestroy()
@Directive({
  selector: '[matchMedia]',
})
export class MediaMatcherDirective {
  private readonly defaultMediaQueryFilterStrategy: MediaQueryFilterStrategy = 'every';

  private mediaQuery = new Subject<MediaQueryBreakpointType>();
  private mediaQuery$ = this.mediaQuery.asObservable();

  private mediaQueryFilterStrategy = new Subject<MediaQueryFilterStrategy>();
  private mediaQueryFilterStrategy$ = this.mediaQueryFilterStrategy.asObservable();

  private hasView = new BehaviorSubject<boolean>(false);
  private hasView$ = this.hasView.asObservable();

  private readonly mediaQueryMatcher = combineLatest([
    this.mediaQuery$,
    this.mediaQueryFilterStrategy$,
  ]).pipe(
    untilDestroyed(this),
    switchMap(([breakpoint, strategy]) =>
      this.breakpointObserver.observe(breakpoint).pipe(
        map((state) => Object.values(state.breakpoints)),
        map((breakpoints) =>
          strategy === 'every'
            ? breakpoints.every((breakpoint) => breakpoint)
            : breakpoints.some((breakpoint) => breakpoint)
        ),
        distinctUntilChanged(),
        withLatestFrom(this.hasView$)
      )
    )
  );

  constructor(
    private readonly viewContainerRef: ViewContainerRef,
    private readonly templateRef: TemplateRef<unknown>,
    private readonly breakpointObserver: BreakpointObserver
  ) {
    this.mediaQueryMatcher.pipe(untilDestroyed(this)).subscribe(([matches, hasView]) => {
      if (matches && !hasView) {
        this.embedView();
      } else if (!matches && hasView) {
        this.clearView();
      }
    });
  }

  @Input()
  set matchMedia(value: MediaQueryBreakpointType) {
    this.mediaQuery.next(value);
  }

  @Input()
  set matchMediaStrategy(strategy: MediaQueryFilterStrategy | undefined) {
    this.mediaQueryFilterStrategy.next(strategy || this.defaultMediaQueryFilterStrategy);
  }

  private embedView() {
    this.viewContainerRef.createEmbeddedView(this.templateRef);
    this.hasView.next(true);
  }

  private clearView() {
    this.viewContainerRef.clear();
    this.hasView.next(false);
  }
}
