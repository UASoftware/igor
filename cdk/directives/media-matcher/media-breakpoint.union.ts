import { LiteralUnion } from 'type-fest';

export type MediaBreakpoint = LiteralUnion<
  | '(orientation: portrait)'
  | '(orientation: landscape)'
  | '(min-width: 320px)'
  | '(min-width: 480px)'
  | '(min-width: 640px)'
  | '(min-width: 768px)'
  | '(min-width: 1024px)'
  | '(min-width: 1200px)'
  | '(min-width: 1400px)'
  | '(min-width: 1536px)'
  | '(max-width: 319px)'
  | '(max-width: 479px)'
  | '(max-width: 639px)'
  | '(max-width: 767px)'
  | '(max-width: 1023px)'
  | '(max-width: 1199px)'
  | '(max-width: 1399px)'
  | '(max-width: 1535px)',
  string
>;
