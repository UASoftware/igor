import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MediaMatcherDirective } from './media-matcher.directive';

const DECLARATION = [MediaMatcherDirective];

@NgModule({
  imports: [CommonModule],
  declarations: DECLARATION,
  exports: DECLARATION,
})
export class MediaMatcherModule {}
