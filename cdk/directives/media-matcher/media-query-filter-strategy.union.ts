export type MediaQueryFilterStrategy = 'every' | 'some';
