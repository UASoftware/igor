import { Observable } from 'rxjs';

import { ExecuteObservable } from './execute-observable';
import { PendingObservable } from './pending-observable';

export interface Query<ModelType, CriteriaType = undefined>
  extends ExecuteObservable<CriteriaType>,
    PendingObservable {
  values$: Observable<ModelType>;
}
