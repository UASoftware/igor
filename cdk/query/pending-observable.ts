import { Observable } from 'rxjs';

export interface PendingObservable {
  pending$: Observable<boolean>;
}
