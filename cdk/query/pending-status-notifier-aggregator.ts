import { PendingStatusNotifier } from './pending-status-notifier';

export abstract class PendingStatusNotifierAggregator {
  public get pending$() {
    return this.pendingStatusNotifier.pending$;
  }

  protected pendingStatusNotifier: PendingStatusNotifier;

  constructor(initialState: boolean) {
    this.pendingStatusNotifier = PendingStatusNotifier.create(initialState);
  }
}
