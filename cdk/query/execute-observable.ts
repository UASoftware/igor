export interface ExecuteObservable<ValueType = undefined> {
  execute(value?: ValueType): void;
}
