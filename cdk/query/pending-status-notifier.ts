import { BehaviorSubject } from 'rxjs';

export class PendingStatusNotifier {
  public get pending$() {
    return this.pendingSubject.asObservable();
  }

  private pendingSubject: BehaviorSubject<boolean>;

  public static create(initialState: boolean) {
    return new PendingStatusNotifier(initialState);
  }

  constructor(initialState: boolean) {
    this.pendingSubject = new BehaviorSubject<boolean>(initialState);
  }

  public start() {
    this.pendingSubject.next(true);
  }

  public finish() {
    this.pendingSubject.next(false);
  }
}
