import { Observable, ReplaySubject, of, EMPTY } from 'rxjs';
import {
  switchMap,
  tap,
  shareReplay,
  catchError,
  startWith,
  distinctUntilChanged,
} from 'rxjs/operators';
import { observableEither as OE } from 'fp-ts-rxjs';
import { CustomError } from 'ts-custom-error';
import { deepEqual } from 'fast-equals';

import { Query } from './query';
import { PendingStatusNotifierAggregator } from './pending-status-notifier-aggregator';

export abstract class BaseQuery<ModelType, CriteriaType = undefined>
  extends PendingStatusNotifierAggregator
  implements Query<ModelType, CriteriaType> {
  public values$: Observable<ModelType>;

  protected readonly bufferSize = 1;

  protected criteriaChangedSubject = new ReplaySubject<CriteriaType>(this.bufferSize);

  constructor() {
    super(true);

    this.values$ = this.getCriteriaChangedSubject();
  }

  public execute(value?: CriteriaType): void {
    this.criteriaChangedSubject.next(value);
  }

  protected getCriteriaChangedSubject() {
    return this.criteriaChangedSubject.pipe(
      startWith(undefined),
      distinctUntilChanged((previous, current) => this.compareChanges(previous, current)),
      tap(() => this.pendingStatusNotifier.start()),
      switchMap((criteria) => this.handleRequest(criteria)),
      tap(() => this.pendingStatusNotifier.finish()),
      distinctUntilChanged((previous, current) => this.compareChanges(previous, current)),
      shareReplay({ bufferSize: this.bufferSize, refCount: true })
    );
  }

  protected compareChanges<T>(previous: T, current: T) {
    return deepEqual(previous, current);
  }

  protected handleRequest(criteria?: CriteriaType): Observable<ModelType> {
    return this.onQuery(criteria).pipe(
      OE.fold(
        () => {
          this.pendingStatusNotifier.finish();
          return EMPTY;
        },
        (model: ModelType) => of(model)
      ),
      catchError(() => {
        this.pendingStatusNotifier.finish();
        return EMPTY;
      })
    );
  }

  protected abstract onQuery(criteria?: CriteriaType): OE.ObservableEither<CustomError, ModelType>;
}
