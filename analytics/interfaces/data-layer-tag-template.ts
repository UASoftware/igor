import { InjectionToken } from '@angular/core';
import { Observable } from 'rxjs';

import { TagObject } from '../types';

export interface DataLayerTagTemplate {
  key: string;
  template: TagObject;
}

export const DATA_LAYER_TAG_TEMPLATE_TOKEN = new InjectionToken<Observable<DataLayerTagTemplate>>(
  'DataLayerTagTemplate'
);
