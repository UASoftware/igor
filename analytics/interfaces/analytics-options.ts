export interface AnalyticsOptions {
  gtmAuth?: string;
  gtmPreview?: string;
  enableTracing?: boolean;
}
