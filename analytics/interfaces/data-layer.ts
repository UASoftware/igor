import { TagObject } from '../types/tag.type';

export interface DataLayer extends Array<TagObject> {}
