import { AnalyticsAction, AnalyticsCategory, TagObject } from '../types';

export interface TrackingEvent {
  key?: string;
  action?: AnalyticsAction;
  category?: AnalyticsCategory;
  label?: string;
  value?: string;
  props?: TagObject;
}
