import { Directive, Optional, Input, HostListener, Inject } from '@angular/core';

import { AnalyticsEventCategoryDirective } from './analytics-event-category.directive';
import { AnalyticsAction, AnalyticsBind } from '../types';
import { ANALYTICS_OPTIONS_TOKEN } from '../tokens';
import { AnalyticsOptions, TrackingEvent } from '../interfaces';
import { GoogleTagManagerService } from '../services';

@Directive({
  selector: `[gaEvent]`,
  exportAs: 'gaEvent',
})
export class AnalyticsEventDirective {
  constructor(
    @Optional() private analyticsCategoryDirective: AnalyticsEventCategoryDirective,
    @Inject(ANALYTICS_OPTIONS_TOKEN) private options: AnalyticsOptions,
    private gtmService: GoogleTagManagerService,
    @Inject(ENV_TOKEN) private environment: Environment
  ) {}

  @Input() gaBind: AnalyticsBind = 'click';
  @Input() gaEvent: AnalyticsAction = 'click';
  @Input() gaAction?: AnalyticsAction;
  @Input() gaLabel?: string;
  @Input() gaValue?: string;
  @Input() gaKey?: string;
  @Input() gaProps?: string;

  @HostListener('click')
  onClick() {
    if (this.gaBind === 'click') {
      this.trigger();
    }
  }

  @HostListener('focus')
  onFocus() {
    if (this.gaBind === 'focus') {
      this.trigger();
    }
  }

  @HostListener('blur')
  onBlur() {
    if (this.gaBind === 'blur') {
      this.trigger();
    }
  }

  private trigger() {
    try {
      if (!this.gaAction && !this.gaEvent) {
        throw new Error('You must provide either gaAction or gaEvent attribute.');
      }
      this.gtmService.trackEvent({
        key: this.gaKey,
        action: this.gaAction || this.gaEvent,
        category: this.analyticsCategoryDirective?.gaCategory,
        label: this.gaLabel,
        value: this.gaValue,
        props: this.gaProps ? JSON.parse(this.gaProps) : undefined,
      } as TrackingEvent);
    } catch (err) {
      this.throw(err);
    }
  }

  private throw(err: Error) {
    if ((!this.environment.production || this.options.enableTracing) && console && console.warn) {
      console.warn(err);
    }
  }
}
