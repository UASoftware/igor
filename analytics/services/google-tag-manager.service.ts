import { Inject, Injectable, OnDestroy } from '@angular/core';
import { forkJoin, Observable, of, Subject, Unsubscribable } from 'rxjs';
import { map, switchMap, take, tap } from 'rxjs/operators';

import {
  DATA_LAYER_TEMPLATE_KEY_MATCHER_TOKEN,
  DATA_LAYER_TOKEN,
  DataLayerTemplateKeyMatcher,
  TAG_REFORMER_TOKEN,
  TagReformer,
  EVENT_NAME_HANDLER_TOKEN,
  EventNameHandler,
} from '../tokens';
import { DataLayerTagTemplate, DataLayer, TrackingEvent } from '../interfaces';
import { TagObject } from '../types';

@Injectable({
  providedIn: 'root',
})
export class GoogleTagManagerService implements OnDestroy {
  private tagTemplates$: Observable<DataLayerTagTemplate>[] = [];

  private templatesSubscription: Unsubscribable | null = null;

  private trackingSubject = new Subject<TrackingEvent>();

  constructor(
    @Inject(DATA_LAYER_TOKEN)
    private readonly dataLayer: DataLayer | null,
    @Inject(DATA_LAYER_TEMPLATE_KEY_MATCHER_TOKEN)
    private dataLayerTemplateKeyMatcher: DataLayerTemplateKeyMatcher,
    @Inject(TAG_REFORMER_TOKEN)
    private tagReformer: TagReformer,
    @Inject(EVENT_NAME_HANDLER_TOKEN)
    private eventNameHandler: EventNameHandler,
    @Inject(ENV_TOKEN) private environment: Environment
  ) {}

  ngOnDestroy() {
    this.trackingSubject.complete();

    if (this.templatesSubscription) {
      this.templatesSubscription.unsubscribe();
    }
  }

  setTagTemplates(tagTemplates: Observable<DataLayerTagTemplate>[]) {
    this.tagTemplates$ = [...tagTemplates];

    this.templatesSubscription = this.getTrackingEventsObservable().subscribe();
  }

  pushTag(tag: TagObject) {
    if (this.dataLayer) {
      tag = this.eventNameHandler(tag);

      this.dataLayer.push(tag);
      this.logTagObject(tag);
    }
  }

  trackEvent(event: TrackingEvent) {
    if (event.key) {
      this.trackingSubject.next(event);
    } else {
      this.mapEventToTagObjectAndPush(event);
    }
  }

  trackNavigation(url: string) {
    this.trackingSubject.next({ key: url } as TrackingEvent);
  }

  private getTrackingEventsObservable() {
    return this.trackingSubject.pipe(
      switchMap((event) => forkJoin([of(event), this.getDataLayerTemplates()])),
      map(([event, templates]) => ({
        event,
        tags: this.getTagObjectsByMatching(templates, event.key),
      })),
      map(({ event, tags }) => this.reformTagObjectsBy(event, tags)),
      tap((tags) => this.pushTagObjects(tags))
    );
  }

  private getDataLayerTemplates(): Observable<DataLayerTagTemplate[]> {
    return this.tagTemplates$ ? forkJoin(this.tagTemplates$).pipe(take(1)) : of([]);
  }

  private getTagObjectsByMatching(templates: DataLayerTagTemplate[], key?: string) {
    return templates
      .filter((template) => this.dataLayerTemplateKeyMatcher(template, key))
      .map((template) => template.template);
  }

  private reformTagObjectsBy(event: TrackingEvent, tags: TagObject[]) {
    return tags.map((tag) => this.tagReformer(event, tag));
  }

  private pushTagObjects(tags: TagObject[]) {
    tags.forEach((tag) => this.pushTag(tag));
  }

  private mapEventToTagObjectAndPush(event: TrackingEvent) {
    this.pushTag(this.tagReformer(event));
  }

  private logTagObject(tag: TagObject) {
    if (this.environment.analytics.logging) {
      const eventName = tag['event'];
      console.groupCollapsed('GTM', `[${eventName ?? ''}]`);
      console.log(JSON.parse(JSON.stringify(tag)));
      console.groupEnd();
    }
  }
}
