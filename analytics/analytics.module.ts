import {
  APP_BOOTSTRAP_LISTENER,
  APP_INITIALIZER,
  ModuleWithProviders,
  NgModule,
} from '@angular/core';
import { DOCUMENT } from '@angular/common';

import { AnalyticsOptions } from './interfaces';
import { ANALYTICS_OPTIONS_TOKEN, DATA_LAYER_TOKEN, EVENT_NAME_HANDLER_TOKEN } from './tokens';
import { GoogleTagManagerService } from './services';
import { defaultEventNameHandler } from './util';
import { googleTagManagerInitializerFactory } from './gtm-initializer';
import { routerListenerInitializerFactory } from './router-listener-initializer';
import { AnalyticsEventDirective } from './directives/analytics-event.directive';
import { AnalyticsEventCategoryDirective } from './directives/analytics-event-category.directive';

@NgModule({
  declarations: [AnalyticsEventDirective, AnalyticsEventCategoryDirective],
  exports: [AnalyticsEventDirective, AnalyticsEventCategoryDirective],
})
export class AnalyticsModule {
  static forRoot(options: AnalyticsOptions): ModuleWithProviders<AnalyticsModule> {
    return {
      ngModule: AnalyticsModule,
      providers: [
        {
          provide: EVENT_NAME_HANDLER_TOKEN,
          useFactory: defaultEventNameHandler,
          deps: [ENV_TOKEN],
        },
        {
          provide: ANALYTICS_OPTIONS_TOKEN,
          useValue: options,
        },
        {
          provide: APP_INITIALIZER,
          multi: true,
          useFactory: googleTagManagerInitializerFactory,
          deps: [ANALYTICS_OPTIONS_TOKEN, DOCUMENT, DATA_LAYER_TOKEN, ENV_TOKEN],
        },
        {
          provide: APP_BOOTSTRAP_LISTENER,
          multi: true,
          useFactory: routerListenerInitializerFactory,
          deps: [GoogleTagManagerService],
        },
      ],
    };
  }
}
