import { LiteralUnion } from 'type-fest';

export type AnalyticsCategory = LiteralUnion<
  'navigation' | 'action' | 'download' | 'color',
  string
>;
