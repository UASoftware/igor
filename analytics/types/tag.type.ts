import { Task } from 'fp-ts/lib/Task';

export type TagArray = Array<TagValue>;

export type TagObject = { [Key in string]: TagValue };

export type TagValue = string | number | boolean | TagObject | TagArray | Task<TagValue>;
