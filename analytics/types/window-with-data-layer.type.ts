import { DataLayer } from '../interfaces/data-layer';

export type WindowWithDataLayer = Window & {
  dataLayer?: DataLayer | null;
};
