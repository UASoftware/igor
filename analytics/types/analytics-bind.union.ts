import { LiteralUnion } from 'type-fest';

export type AnalyticsBind = LiteralUnion<'click' | 'focus' | 'blur', string>;
