import { LiteralUnion } from 'type-fest';

export type AppSectionName = LiteralUnion<
  | 'Alpha'
  | 'Beta'
  | 'Gamma'
  string
>;
