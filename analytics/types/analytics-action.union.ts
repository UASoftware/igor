import { LiteralUnion } from 'type-fest';

export type AnalyticsAction = LiteralUnion<
  'click' | 'promotionClick' | 'productClick' | 'addToCart',
  string
>;
