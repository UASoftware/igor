import { ComponentRef } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';

import { GoogleTagManagerService } from './services';

export const routerListenerInitializerFactory = (gtmService: GoogleTagManagerService) => async (
  componentRef: ComponentRef<unknown>
) => {
  const router = componentRef.injector.get(Router);
  const subscription = router.events.subscribe((event) => {
    if (event instanceof NavigationEnd) {
      gtmService.trackNavigation(event.urlAfterRedirects);
    }
  });
  componentRef.onDestroy(() => subscription.unsubscribe());
};
