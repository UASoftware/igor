import { InjectionToken, inject, PLATFORM_ID } from '@angular/core';
import { isPlatformBrowser } from '@angular/common';

export function getWindow(platformId: Object) {
  return isPlatformBrowser(platformId) ? window : null;
}

export const WINDOW_TOKEN = new InjectionToken<Window | null>('GlobalWindowObject', {
  providedIn: 'root',
  factory: () => getWindow(inject(PLATFORM_ID)),
});
