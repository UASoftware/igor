import { InjectionToken, inject } from '@angular/core';

import { DataLayer } from '../interfaces/data-layer';
import { WindowWithDataLayer } from '../types/window-with-data-layer.type';
import { WINDOW_TOKEN } from './window.token';

export function getDataLayer(window: WindowWithDataLayer | null): DataLayer | null {
  return window ? (window['dataLayer'] = window['dataLayer'] || []) : null;
}

export const DATA_LAYER_TOKEN = new InjectionToken<DataLayer | null>('DataLayer', {
  providedIn: 'root',
  factory: () => getDataLayer(inject(WINDOW_TOKEN)),
});
