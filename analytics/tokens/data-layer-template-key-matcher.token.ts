import { InjectionToken } from '@angular/core';
import { Match, match } from 'path-to-regexp';

import { DataLayerTagTemplate } from '../interfaces/data-layer-tag-template';

export type DataLayerTemplateKeyMatcher = (
  template: DataLayerTagTemplate,
  key?: string
) => Match<object> | boolean;

export const DATA_LAYER_TEMPLATE_KEY_MATCHER_TOKEN = new InjectionToken<
  DataLayerTemplateKeyMatcher
>('DataLayerTemplateKeyMatcher', {
  providedIn: 'root',
  factory: () => (template: DataLayerTagTemplate, key?: string) =>
    key ? match(`${template.key}(\\?.*)?`)(key) : false,
});
