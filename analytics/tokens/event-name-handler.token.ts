import { InjectionToken } from '@angular/core';

import { TagObject } from '../types';

export type EventNameHandler = (sourceTag: TagObject) => TagObject;

export const EVENT_NAME_HANDLER_TOKEN = new InjectionToken<EventNameHandler>('EventNameHandler');
