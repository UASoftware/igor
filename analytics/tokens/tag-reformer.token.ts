import { InjectionToken } from '@angular/core';

import { TrackingEvent } from '../interfaces';
import { TagObject } from '../types';

export type TagReformer = (event: TrackingEvent, tag?: TagObject) => TagObject;

export const TAG_REFORMER_TOKEN = new InjectionToken<TagReformer>('TagReformer', {
  providedIn: 'root',
  factory: () => (event: TrackingEvent, tag?: TagObject) =>
    ({
      category: event.category,
      event: event.action,
      action: event.action,
      label: event.label,
      value: event.value,
      ...event.props,
      ...tag,
    } as TagObject),
});
