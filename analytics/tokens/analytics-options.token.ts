import { InjectionToken } from '@angular/core';

import { AnalyticsOptions } from '../interfaces/analytics-options';

export const ANALYTICS_OPTIONS_TOKEN = new InjectionToken<AnalyticsOptions>('AnalyticsOptions');
