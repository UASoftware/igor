import { createAction, props } from '@ngrx/store';

export const loadPromotionalImages = createAction('[PromotionalImages] Load Promotional Images');

export const loadPromotionalImagesSuccess = createAction(
  '[PromotionalImages] Load Promotional Images Success',
  props<{ data: PromotionalImage[] }>()
);

export const loadPromotionalImagesFailure = createAction(
  '[PromotionalImages] Load Promotional Images Failure',
  props<{ error: string }>()
);

export const loadPromotionalImagesStatus = createAction(
  '[PromotionalImages] Load Promotional Images Status',
  props<{ status: QueryOutputStatus }>()
);
