import { Injectable } from '@angular/core';
import { select, Store } from '@ngrx/store';

import * as fromPromotionalImages from './promotional-images.reducer';
import * as PromotionalImagesActions from './promotional-images.actions';
import * as PromotionalImagesSelectors from './promotional-images.selectors';

@Injectable()
export class PromotionalImagesFacade implements PromotionImagesRepository {
  loading$ = this.store.pipe(select(PromotionalImagesSelectors.selectPromotionalImagesLoading));
  status$ = this.store.pipe(select(PromotionalImagesSelectors.selectPromotionalImagesStatus));
  error$ = this.store.pipe(select(PromotionalImagesSelectors.selectPromotionalImagesError));
  all$ = this.store.pipe(select(PromotionalImagesSelectors.selectAllPromotionalImages));
  activeOnly$ = this.store.pipe(select(PromotionalImagesSelectors.selectActivePromotionalImages));

  constructor(private store: Store<fromPromotionalImages.PromotionalImagesPartialState>) {}

  fetchAll() {
    this.store.dispatch(PromotionalImagesActions.loadPromotionalImages());
  }
}
