import { Injectable } from '@angular/core';
import { Action } from '@ngrx/store';
import { createEffect, Actions, ofType, OnInitEffects } from '@ngrx/effects';
import { switchMap, flatMap } from 'rxjs/operators';
import { query } from 'rx-query';

import { PromotionalImageDataService } from '../../services';
import * as PromotionalImagesActions from './promotional-images.actions';

@Injectable()
export class PromotionalImagesEffects implements OnInitEffects {
  loadPromotionalImages$ = createEffect(() =>
    this.actions$.pipe(
      ofType(PromotionalImagesActions.loadPromotionalImages),
      switchMap(() =>
        query(
          PromotionalImagesActions.loadPromotionalImages.type,
          () => this.promotionalImageDataService.getPromotionalImages(),
          { refetchOnWindowFocus: false }
        ).pipe(
          flatMap((queryOutput) =>
            getActionsDependingOnQueryOutput<PromotionalImage[]>(
              PromotionalImagesActions.loadPromotionalImagesSuccess,
              PromotionalImagesActions.loadPromotionalImagesFailure,
              PromotionalImagesActions.loadPromotionalImagesStatus
            )(queryOutput)
          )
        )
      )
    )
  );

  ngrxOnInitEffects(): Action {
    return PromotionalImagesActions.loadPromotionalImages();
  }

  constructor(
    private actions$: Actions,
    private promotionalImageDataService: PromotionalImageDataService
  ) {}
}
