import { createReducer, on, Action } from '@ngrx/store';
import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';

import * as PromotionalImagesActions from './promotional-images.actions';

export const PROMOTIONAL_IMAGES_FEATURE_KEY = 'promotionalImages';

export interface State extends EntityState<PromotionalImage>, StatusErrorState {
  selectedId?: number;
}

export interface PromotionalImagesPartialState {
  readonly [PROMOTIONAL_IMAGES_FEATURE_KEY]: State;
}

export const promotionalImagesAdapter: EntityAdapter<PromotionalImage> = createEntityAdapter<PromotionalImage>();

export const initialState: State = promotionalImagesAdapter.getInitialState({
  status: 'idle',
});

const promotionalImagesReducer = createReducer(
  initialState,
  on(PromotionalImagesActions.loadPromotionalImagesSuccess, (state, { data }) =>
    promotionalImagesAdapter.setAll(data, { ...state, error: undefined })
  ),
  on(PromotionalImagesActions.loadPromotionalImagesFailure, (state, { error }) => ({
    ...state,
    error,
  })),
  on(PromotionalImagesActions.loadPromotionalImagesStatus, (state, { status }) => ({
    ...state,
    status,
  }))
);

export function reducer(state: State | undefined, action: Action) {
  return promotionalImagesReducer(state, action);
}
