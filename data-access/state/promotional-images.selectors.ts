import { createFeatureSelector, createSelector } from '@ngrx/store';

import {
  PROMOTIONAL_IMAGES_FEATURE_KEY,
  State,
  PromotionalImagesPartialState,
  promotionalImagesAdapter,
} from './promotional-images.reducer';

export const selectPromotionalImages = createFeatureSelector<PromotionalImagesPartialState, State>(
  PROMOTIONAL_IMAGES_FEATURE_KEY
);

const { selectAll, selectEntities } = promotionalImagesAdapter.getSelectors();

export const selectPromotionalImagesStatus = createSelector(
  selectPromotionalImages,
  (state: State) => state.status
);

export const selectPromotionalImagesLoading = createSelector(selectPromotionalImages, getIsLoading);

export const selectPromotionalImagesError = createSelector(selectPromotionalImages, getError);

export const selectAllPromotionalImages = createSelector(selectPromotionalImages, (state: State) =>
  selectAll(state)
);

export const selectPromotionalImagesEntities = createSelector(
  selectPromotionalImages,
  (state: State) => selectEntities(state)
);

export const selectActivePromotionalImages = createSelector(selectAllPromotionalImages, (images) =>
  images.filter((image) => image.isActive)
);
