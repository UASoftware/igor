import { morphism, createSchema } from 'morphism';
import { JsonObject } from 'type-fest';

import { plainToCustomerGroupsSchema } from './plain-to-customer-groups.schema';

export const plainToPromotionalImageSchema = createSchema<PromotionalImage, JsonObject>({
  id: 'id',
  imageLarge: 'image_large',
  imageSmall: 'image_small',
  isActive: ({ is_active }) => is_active === 1,
  urlLink: 'url_link',
  positionZone: 'position_zone',
  platformId: 'platforms_id',
  customerGroups: ({ customers_groups }) =>
    morphism(plainToCustomerGroupsSchema, customers_groups as JsonObject[]),
});
