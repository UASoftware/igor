import { createSchema } from 'morphism';
import { JsonObject } from 'type-fest';

export const plainToCustomerGroupsSchema = createSchema<CustomerGroup, JsonObject>({
  id: 'id',
  name: 'name',
  platformId: 'platforms_id',
  mailRecognition: 'mail_recognition',
  externalReference: 'external_reference',
  boColor: 'bo_color',
  ebpBillingGeneralAccount: 'EBP_billing_general_account',
  ebpBillingSaleAccountFrance: 'EBP_billing_sale_account_france',
  ebpBillingSaleAccountUE: 'EBP_billing_sale_account_UE',
  ebpBillingSaleAccountOutsideUE: 'EBP_billing_sale_account_outside_UE',
  ebpBillingSaleAccountDomWithVAT: 'EBP_billing_sale_account_dom_with_VAT',
  ebpBillingSaleAccountDomTomNoVAT: 'EBP_billing_sale_account_dom_tom_no_VAT',
  ebpBillingSaleAccountUEWithVAT: 'EBP_billing_sale_account_UE_with_VAT',
  pivotPromotionalImageId: '_pivot_promotional_images_id',
  pivotCustomerGroupId: '_pivot_customers_groups_id',
});
