import { InjectionToken } from '@angular/core';
import { morphism } from 'morphism';
import { JsonObject } from 'type-fest';

import { plainToPromotionalImageSchema } from './schemas';

export type PlainToPromotionalImageArrayMapper = ResponseDataMapper<PromotionalImage[]>;

export const mapPlainToPromotionalImageArray: PlainToPromotionalImageArrayMapper = (from) =>
  morphism(plainToPromotionalImageSchema, from as JsonObject[]);

export const PLAIN_TO_PROMOTIONAL_IMAGE_ARRAY_MAPPER_TOKEN = new InjectionToken<PlainToPromotionalImageArrayMapper>(
  'PlainToPromotionalImageArrayMapper',
  {
    factory: () => mapPlainToPromotionalImageArray,
  }
);
