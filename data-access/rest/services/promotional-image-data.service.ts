import { Inject, Injectable } from '@angular/core';

import {
  PLAIN_TO_PROMOTIONAL_IMAGE_ARRAY_MAPPER_TOKEN,
  PlainToPromotionalImageArrayMapper,
} from '../mappers';

@Injectable({ providedIn: 'root' })
export class PromotionalImageDataService {
  constructor(
    private dataSource: RESTDataSource,
    @Inject(PLAIN_TO_PROMOTIONAL_IMAGE_ARRAY_MAPPER_TOKEN)
    private plainToPromotionalImageArrayMapper: PlainToPromotionalImageArrayMapper,
    @Inject(QUERY_PROMOTIONAL_IMAGES_URL_PROVIDER_TOKEN)
    private queryPromotionalImagesUrlProvider: QueryPromotionalImagesUrlProvider
  ) {}

  getPromotionalImages() {
    return this.dataSource.query(
      this.queryPromotionalImagesUrlProvider(),
      this.plainToPromotionalImageArrayMapper
    );
  }
}
