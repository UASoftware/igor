import { NgModule, Optional, SkipSelf } from '@angular/core';
import { ApolloModule, APOLLO_OPTIONS } from 'apollo-angular';
import { ApolloLink, split, from, concat } from 'apollo-link';
import { HttpLinkModule, HttpLink } from 'apollo-angular-link-http';
import { WebSocketLink } from 'apollo-link-ws';
import { onError } from 'apollo-link-error';
import { getMainDefinition } from 'apollo-utilities';
import { InMemoryCache, IntrospectionFragmentMatcher } from 'apollo-cache-inmemory';
import apolloLogger from 'apollo-link-logger';

export function initApollo(httpLinkService: HttpLink) {
  const fragmentMatcher = new IntrospectionFragmentMatcher({
    introspectionQueryResultData: {
      __schema: {
        types: [],
      },
    },
  });

  const cache = new InMemoryCache({ addTypename: true, fragmentMatcher });

  const errorLink = onError(({ graphQLErrors, networkError }) => {
    if (graphQLErrors) {
      graphQLErrors.map(({ message, locations, path }) =>
        console.warn(`[GraphQL error]: Message: ${message}, Location: ${locations}, Path: ${path}`)
      );
    }
    if (networkError) {
      const n: any = networkError;
      if (n.error && n.error.errors && n.error.errors.length > 0) {
        const message = n.error.errors[0].message;
        console.warn(`[Network error]: ${message}`);
      } else {
        console.warn(`[Network error]: ${n.message}`);
      }
    }
  });

  const httpLink = httpLinkService.create({
    uri: environment.HTTP_GRAPHQL_URL,
  });

  const wsLink = new WebSocketLink({
    uri: environment.WS_GRAPHQL_URL,
    options: {
      reconnect: true,
      timeout: environment.WS_TIMEOUT,
      lazy: true,
    },
  });

  const transportLink = split(
    ({ query }) => {
      const { kind, operation } = getMainDefinition(query) as any;
      return kind === 'OperationDefinition' && operation === 'subscription';
    },
    wsLink,
    httpLink
  );

  const link = from([errorLink, transportLink].filter((x) => !!x) as ApolloLink[]);

  return {
    link: environment.production ? link : concat(apolloLogger, link),
    cache,
    ssrMode: false,
    connectToDevTools: !environment.production,
    queryDeduplication: true,
  };
}

@NgModule({
  imports: [ApolloModule, HttpLinkModule],
  providers: [
    ProjectDataService,
    CollectionImageDataService,
    CollectionSoundDataService,
    ImageDataService,
    SoundDataService,
    {
      provide: APOLLO_OPTIONS,
      useFactory: initApollo,
      deps: [HttpLink],
    },
  ],
})
export class BackendModule {
  constructor(
    @Optional()
    @SkipSelf()
    parentModule: BackendModule
  ) {
    if (parentModule) {
      throw new Error('BackendModule is already loaded. Import it in the CoreModule only');
    }
  }
}
