import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import {
  CollectionImage,
  DeleteEntityResult,
  CreateCollectionImageGQL,
  UpdateCollectionImageGQL,
  DeleteCollectionImageGQL,
  CollectionImagesGQL,
  CollectionImageGQL,
} from '@common/schemas/api-types';

@Injectable()
export class CollectionImageDataService {
  constructor(
    private readonly createCollection: CreateCollectionImageGQL,
    private readonly updateCollection: UpdateCollectionImageGQL,
    private readonly deleteCollection: DeleteCollectionImageGQL,
    private readonly collections: CollectionImagesGQL,
    private readonly collection: CollectionImageGQL
  ) {}

  create(entity: Partial<CollectionImage>): Observable<CollectionImage> {
    return this.createCollection
      .mutate(
        {
          input: {
            name: entity.name || null,
            description: entity.description || null,
            duration: entity.duration || null,
          },
        },
        {
          refetchQueries: [
            {
              query: this.collections.document,
            },
          ],
        }
      )
      .pipe(map(({ data }) => data.createCollectionImage));
  }

  update(changes: Partial<CollectionImage>): Observable<CollectionImage> {
    return this.updateCollection
      .mutate({
        input: {
          id: changes.id,
          name: changes.name || null,
          description: changes.description || null,
          duration: changes.duration || null,
        },
      })
      .pipe(map(({ data }) => data.updateCollectionImage));
  }

  delete(id: number): Observable<DeleteEntityResult> {
    return this.deleteCollection
      .mutate(
        { id },
        {
          refetchQueries: [
            {
              query: this.collections.document,
            },
          ],
        }
      )
      .pipe(map(({ data }) => data.deleteCollectionImage));
  }

  watchQueryAll(): Observable<CollectionImage[]> {
    return this.collections
      .watch()
      .valueChanges.pipe(map(({ data }) => data && data.collectionImages));
  }

  watchQueryById(id: number): Observable<CollectionImage> {
    return this.collection
      .watch({ id })
      .valueChanges.pipe(map(({ data }) => data && data.collectionImage));
  }
}
