import { QueryOutputStatus } from './query-output-status.union';

export interface StatusErrorState {
  status: QueryOutputStatus;
  error?: string;
}
