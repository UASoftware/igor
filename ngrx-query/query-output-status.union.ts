export type QueryOutputStatus = LiteralUnion<
  'idle' | 'success' | 'error' | 'loading' | 'refreshing' | 'mutating' | 'mutate-error',
  string
>;
