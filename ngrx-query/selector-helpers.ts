import { StatusErrorState } from './status-error.state';

export const getIsLoading = <StateType extends StatusErrorState>(state: StateType) =>
  state.status === 'loading';

export const getError = <StateType extends StatusErrorState>(state: StateType) =>
  state.status === 'error' ? state.error : undefined;
