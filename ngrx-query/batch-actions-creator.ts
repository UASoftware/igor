import { ActionCreator, Action } from '@ngrx/store';
import { TypedAction } from '@ngrx/store/src/models';
import { QueryOutput } from 'rx-query';
import { CustomError } from 'ts-custom-error';

import { QueryOutputStatus } from './query-output-status.union';

export const getActionsDependingOnQueryOutput = <ResultType>(
  successAction: ActionCreator<
    string,
    (props: {
      data: ResultType;
    }) => {
      data: ResultType;
    } & TypedAction<string>
  >,
  failureAction: ActionCreator<
    string,
    (props: {
      error: string;
    }) => {
      error: string;
    } & TypedAction<string>
  >,
  statusAction: ActionCreator<
    string,
    (props: {
      status: QueryOutputStatus;
    }) => {
      status: QueryOutputStatus;
    } & TypedAction<string>
  >
) => (queryOutput: QueryOutput<ResultType>) => {
  const actions: Action[] = [
    statusAction({
      status: queryOutput.status,
    }),
  ];

  switch (queryOutput.status) {
    case 'success':
      actions.push(
        successAction({
          data: queryOutput.data as ResultType,
        })
      );
      break;
    case 'error':
      actions.push(
        failureAction({
          error: (queryOutput.error as CustomError).message as string,
        })
      );
      break;
    default:
      break;
  }

  return actions;
};
