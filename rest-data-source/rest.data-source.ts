export type MutateMethod = LiteralUnion<'POST' | 'PUT' | 'PATCH', string>;

@Injectable({ providedIn: 'root' })
export class RESTDataSource {
  constructor(private http: HttpClient) {}

  query<ResultType>(
    url: string,
    mapToResultType: ResponseDataMapper<ResultType>,
    options?: JsonObject
  ): Observable<ResultType> {
    return this.http.get<HttpResultType>(url, options).pipe(
      switchMap((response) =>
        iif(
          () => response === undefined || response === null,
          throwError(new InvalidResponseError()),
          of(mapToResultType(response))
        )
      ),
      catchError(handleResponseError)
    );
  }

  mutate<ResultType, DataType>(
    method: MutateMethod,
    url: string,
    data: DataType,
    mapToResultType?: ResponseDataMapper<ResultType>,
    options?: JsonObject
  ): RequestResultType<ResultType> {
    return this.http
      .request<HttpResultType>(method, url, {
        ...options,
        body: data,
      })
      .pipe(
        map((response) => (mapToResultType ? mapToResultType(response) : response)),
        catchError(handleResponseError)
      );
  }
}
