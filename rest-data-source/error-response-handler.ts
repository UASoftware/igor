import {
  BadGatewayError,
  ForbiddenError,
  NetworkError,
  UnauthorizedError,
  UnhandledHttpError,
  UrlNotFoundError,
} from './errors';

export const handleResponseError = (error: HttpErrorResponse) => {
  let resultError: CustomError;

  if (error.error instanceof ErrorEvent) {
    console.error('An error occurred:', error.error.message);
    resultError = new NetworkError(error.error.message);
  } else {
    console.error(`Server returned code ${error.status}, body was: ${error.error}`);
    switch (error.status) {
      case 401:
        resultError = new UnauthorizedError();
        break;
      case 403:
        resultError = new ForbiddenError();
        break;
      case 404:
        resultError = new UrlNotFoundError();
        break;
      case 502:
        resultError = new BadGatewayError();
        break;
      default:
        resultError = new UnhandledHttpError(error);
        break;
    }
  }
  return throwError(resultError);
};
