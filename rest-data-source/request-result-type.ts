export type HttpResultType = JsonObject | JsonArray | undefined | null;

export type RequestResultType<ResultType> = Observable<ResultType | HttpResultType>;
