export type ResponseDataMapper<ResultType> = (from: HttpResultType) => ResultType;
